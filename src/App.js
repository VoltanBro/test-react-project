import './App.css';
import React, {Component} from 'react';
import ReactTooltip from "react-tooltip";

const GrayStyle = {
    backgroundColor: "#808080",
}

const BlueStyle = {
    backgroundColor: "#0000FF",
}

const GreenStyle = {
    backgroundColor: "#00FF00",
}

const RedStyle = {
    backgroundColor: "#FF0000",
}

const styleFunction = (usersFiltered) => {

    switch (true) {
        case (usersFiltered.length >= 0 && usersFiltered .length <= 2):
            return GrayStyle;

        case (usersFiltered.length >= 3 && usersFiltered.length <= 6):
            return BlueStyle;
        case (usersFiltered.length >= 7 && usersFiltered.length <= 10):
            return GreenStyle;
        default:
            return RedStyle;
    }
}

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            months: [
                {num: 0, name: "January"},
                {num: 1, name: "February"},
                {num: 2, name: "March"},
                {num: 3, name: "April"},
                {num: 4, name: "May"},
                {num: 5, name: "June"},
                {num: 6, name: "July"},
                {num: 7, name: "August"},
                {num: 8, name: "September"},
                {num: 9, name: "October"},
                {num: 10, name: "November"},
                {num: 11, name: "December"}
            ],
            users: [],

            place: "bottom",
            type: "dark",
            effect: "float",
            condition: false
        };
    }

    componentDidMount() {
        fetch('https://yalantis-react-school-api.yalantis.com/api/task0/users')
            .then(response => response.json())
            .then(data => this.setState({users: data}));
    }


    render() {
        const {users, months} = this.state;

        return (
            <ul> {
                months.map(function (month) {
                    let usersFiltered = users.filter(user => new Date(user.dob).getMonth() === month.num);

                    return (< li key={month.num} >
                        <ReactTooltip
                            place="bottom" type="dark" effect="float"
                        />
                        <a style={styleFunction(usersFiltered)}
                           data-tip={usersFiltered.map(user => user.firstName + " " + user.lastName)}>
                            {month.name}
                        </a>
                    </li>)
                })
            }
            </ul>
        );
    }
}
export default App;